package opentelemetry

import (
	"context"
	"errors"
	"gitlab.com/navenest/golang-library/pkg/logger/log"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlpmetric/otlpmetricgrpc"
	"go.opentelemetry.io/otel/exporters/otlp/otlpmetric/otlpmetrichttp"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/metric"
	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
	"go.uber.org/zap"
	"os"
	"strings"
	"time"
)

var (
	MeterProvider *metric.MeterProvider
	TraceProvider *trace.TracerProvider
	otelProtocol  = os.Getenv("OTEL_EXPORTER_OTLP_PROTOCOL")
)

// SetupOTelSDK bootstraps the OpenTelemetry pipeline.
// If it does not return an error, make sure to call shutdown for proper cleanup.
func SetupOTelSDK(ctx context.Context, resources *resource.Resource) (shutdown func(context.Context) error, err error) {
	var shutdownFuncs []func(context.Context) error

	// shutdown calls cleanup functions registered via shutdownFuncs.
	// The errors from the calls are joined.
	// Each registered cleanup will be invoked once.
	shutdown = func(ctx context.Context) error {
		var err error
		for _, fn := range shutdownFuncs {
			err = errors.Join(err, fn(ctx))
		}
		shutdownFuncs = nil
		return err
	}

	// handleErr calls shutdown for cleanup and makes sure that all errors are returned.
	handleErr := func(inErr error) {
		err = errors.Join(inErr, shutdown(ctx))
	}

	if resources == nil {
		resources, err = resource.New(
			ctx,
			resource.WithFromEnv(),      // Discover and provide attributes from OTEL_RESOURCE_ATTRIBUTES and OTEL_SERVICE_NAME environment variables.
			resource.WithTelemetrySDK(), // Discover and provide information about the OpenTelemetry SDK used.
			resource.WithProcess(),      // Discover and provide process information.
			resource.WithOS(),           // Discover and provide OS information.
			resource.WithContainer(),    // Discover and provide container information.
			resource.WithHost(),         // Discover and provide host information.
			//resource.WithAttributes(attribute.String("foo", "bar")), // Add custom resource attributes.
			// resource.WithDetectors(thirdparty.Detector{}), // Bring your own external Detector implementation.
		)
	}

	// Set up propagator.
	prop := newPropagator()
	otel.SetTextMapPropagator(prop)

	// Set up trace provider.
	TraceProvider, err = newTraceProvider(ctx, resources)
	if err != nil {
		handleErr(err)
		return
	}
	shutdownFuncs = append(shutdownFuncs, TraceProvider.Shutdown)
	otel.SetTracerProvider(TraceProvider)

	// Set up meter provider.
	MeterProvider, err = newMeterProvider(ctx, resources)
	if err != nil {
		handleErr(err)
		return
	}
	shutdownFuncs = append(shutdownFuncs, MeterProvider.Shutdown)
	otel.SetMeterProvider(MeterProvider)

	return
}

func newPropagator() propagation.TextMapPropagator {
	return propagation.NewCompositeTextMapPropagator(
		propagation.TraceContext{},
		propagation.Baggage{},
	)
}

func newTraceProvider(ctx context.Context, resources *resource.Resource) (*trace.TracerProvider, error) {
	var traceProvider *trace.TracerProvider
	if strings.ToLower(otelProtocol) == "grpc" {
		traceExporter, err := otlptracegrpc.New(ctx)
		if err != nil {
			log.Logger.Error("Unable to create otlp GRPC",
				zap.Error(err))
			return nil, err
		}
		traceProvider = trace.NewTracerProvider(
			trace.WithBatcher(traceExporter,
				// Default is 1m.
				trace.WithBatchTimeout(5*time.Second),
			),
			trace.WithResource(resources),
		)

	} else {
		traceExporter, err := otlptracehttp.New(ctx)
		if err != nil {
			log.Logger.Error("Unable to create otlp HTTP",
				zap.Error(err))
			return nil, err
		}
		traceProvider = trace.NewTracerProvider(
			trace.WithBatcher(traceExporter,
				// Default is 1m.
				trace.WithBatchTimeout(5*time.Second),
			),
			trace.WithResource(resources),
		)
	}

	return traceProvider, nil

}

func newMeterProvider(ctx context.Context, resources *resource.Resource) (*metric.MeterProvider, error) {
	var meterProvider *metric.MeterProvider
	if strings.ToLower(otelProtocol) == "grpc" {
		metricExporter, err := otlpmetricgrpc.New(ctx)
		if err != nil {
			log.Logger.Error("Unable to create otlp GRPC",
				zap.Error(err))
			return nil, err
		}
		meterProvider = metric.NewMeterProvider(
			metric.WithReader(metric.NewPeriodicReader(metricExporter,
				// Default is 1m.
				metric.WithInterval(60*time.Second))),
			metric.WithResource(resources),
		)

	} else {
		metricExporter, err := otlpmetrichttp.New(ctx)
		if err != nil {
			log.Logger.Error("Unable to create otlp GRPC",
				zap.Error(err))
			return nil, err
		}
		meterProvider = metric.NewMeterProvider(
			metric.WithReader(metric.NewPeriodicReader(metricExporter,
				// Default is 1m.
				metric.WithInterval(60*time.Second))),
			metric.WithResource(resources),
		)
	}
	return meterProvider, nil
}
